FROM nginx
RUN chmod g+rwx /var/cache/nginx /var/run /var/log/nginx
RUN rm /etc/nginx/nginx.conf
COPY content /etc/nginx/html/
COPY conf /etc/nginx/
RUN sed -i.bak 's/^user/#user/' /etc/nginx/nginx.conf
EXPOSE 8080